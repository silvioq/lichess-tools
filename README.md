Herramientas Lichess
====================

Conjunto de herramientas diseñadas para el equipo [Clases de ajedrez en vivo del IMD](https://lichess.org/team/clases-de-ajedrez-en-vivo-del-imd).
Las clases en vivo son todos los días a las 19 horas (Hora Argentina) en el canal de twich https://www.twitch.tv/juancruzariastdf
con el CM Juan Cruz Arias e invitados.

Estas herramientas están pensadas para usar amigablemente las [API de Lichess](https://lichess.org/api).

Torneos
-------

El script import.php recibe un conjunto de torneos y devuelve la información de los usuarios involucrados en formato CSV

Requisitos: php 7.4 o superior.

Uso:

```bash
php torneos/import.php IDTorneo1 IDTorneo2 TorneoN
```

Ejercitación
------------

Este script permite la generación de un archivo con la evolución de los ejercicios resueltos por los usuarios.

Requisitos: python 3.4 o superior.

Uso:

```bash
python3 ejercitacion/leer-puzzles.py usuario1,usuario2,...
```

Como salida, se obtiene en el directorio `data` el archivo `stalkeame.csv`.

Restricción: 300 usuarios.

Consideraciones para ejecución en Windows
--------------

Estos scripts fueron diseñados en entorno Linux. Pendiente documentación para ejecución en Windows.


Colaboraciones
--------------

Para colaborar, clonar el proyecto y enviar un merge request.

TODO
----

- [ ] Migrar script de torneos a python3


