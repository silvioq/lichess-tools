import os
from pathlib import Path
import json

# Directorio de datos. Relativo al script actual.
DATA_DIR = "../data"
current_dir = Path(__file__).parent

headers={
   "User-Agent": "IMD Clases en vivo bot (+https://gitlab.com/silvioq/lichess-tools)"
}
if os.environ.get("LICHESS_TOKEN"):
   headers["Authorization"] = "Bearer %s" % os.environ.get("LICHESS_TOKEN")


def read_listaalumnos():
    file_name=os.path.join(current_dir, DATA_DIR, 'lista-alumnos.txt')
    ret = []
    with open(file_name, "r") as f:
        for row in f:
            ret.append(row.lower().strip())

    return ret;


def get_headers(accept="application/json"):
     h = headers
     h["Accept"] = accept
     return h
