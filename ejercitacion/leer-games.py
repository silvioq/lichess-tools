import json
import requests
import os
import re
import sys
from pathlib import Path
from datetime import datetime, timezone, timedelta
from time import sleep
from common import read_listaalumnos, get_headers

# Directorio de datos. Relativo al script actual.
DATA_DIR = "../data"

# Archivo de salida. Ojo! Lo sobreescribe
FILE_OUTPUT = "games.csv"

# Este parámetro es para no bombardear la API de lichess y no bloquee
# Está en segundos
ESPERA_ENTRE_BUSQUEDA = 0.25

# Espera luego de que lichess me diga "too_many_request"
# Está en segundos
ESPERA_ENTRE_429 = 75

# Cantidad de días de antigüedad para generar la salida
DIAS_ANTIGUEDAD = 10

# Validamos el ingreso
if len(sys.argv) != 2:
    print("Uso:")
    print("  %s Lista,de,usuarios,separados,por,comas,sin,espacios\n" % sys.argv[0])
    print("Ej:")
    print("  %s juancruzariastdf,dxargon" % sys.argv[0])
    sys.exit(1)

# Paso todo a archivos
current_dir = Path(__file__).parent
file_output = os.path.join(current_dir, DATA_DIR, FILE_OUTPUT)

# Lista de usuarios a stalkear, separado por comas
users=sys.argv[1]
if users == '-':
   userslist=read_listaalumnos()
else:
   userslist=re.split(',', users.lower())

def build_output():
    fechacorte = datetime.today() - timedelta(DIAS_ANTIGUEDAD)
    fechacorte = fechacorte.replace(hour=0, minute=0, second=0, microsecond=0).timestamp()
    print ("Construyendo el archivo de salida %s" % file_output)
    with open(file_output, "w") as f:
        f.write("Usuario,Fecha,ID,URL,Rated,Variante,Modalidad,Resultado,Ganado,Empate,Perdido,Status,Tiempo,Color,Ranking,RankOponente,RankDiff,Oponente,Movidas\n")
        for u in userslist:
            data = get_data(u)
            for id in data["games"]:
                try:
                    game = data["games"][id]
                    if game["createdAt"]/1000 < fechacorte:
                        continue
                    if "aiLevel" in game["players"]["white"] or "aiLevel" in game["players"]["black"]:
                        print ("Ignoro %s por ser contra la compu."% id)
                        continue
                    if (not "user" in game["players"]["white"]) or (not "user" in game["players"]["black"]):
                        print ("Ignoro %s por no tener rival."% id)
                        continue

                    fecha = datetime.fromtimestamp(game["createdAt"]/1000, timezone.utc).strftime("%Y-%m-%d")
                    if game["rated"]:
                        rated = 1
                    else:
                        rated = 0
                    resultado = '"0,5"'

                    if "winner" in game:
                        win = game["winner"]
                        if game["players"][win]["user"]["id"] == u:
                            resultado = 1
                        else:
                            resultado = 0
                    is_win = is_draw = is_lost = 0
                    if resultado == 1:
                        is_win = 1
                    elif resultado == 0:
                        is_lost = 1
                    else:
                        is_draw = 1

                    segundos = (game["lastMoveAt"] - game["createdAt"]) / 1000.0
                    

                    # convierto a formato español para la interpretación de la importación de datos
                    segundos = f"\"{segundos}\"".replace('.',',')

                    if game["players"]["white"]["user"]["id"] == u:
                        mirank = game["players"]["white"]["rating"]
                        if "ratingDiff" in game["players"]["white"]:
                            rankdiff = game["players"]["white"]["ratingDiff"]
                        else:
                            rankdiff = ""
                        surank = game["players"]["black"]["rating"]
                        oponente = game["players"]["black"]["user"]["id"]
                        color = "Blancas"
                    else:
                        surank = game["players"]["white"]["rating"]
                        mirank = game["players"]["black"]["rating"]
                        if "ratingDiff" in game["players"]["black"]:
                            rankdiff = game["players"]["black"]["ratingDiff"]
                        else:
                            rankdiff = ""
                        oponente = game["players"]["white"]["user"]["id"]
                        color = "Negras"
                except KeyError as e:
                    print("La partida %s de %s tiene problema: %s"% (u, id, str(e)))


                movidas = re.split('\s', game["moves"])
                movidas = (len(movidas)/2)+0.5
                movidas = int(movidas)
                url = "https://lichess.org/%s"%id
                f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (u, fecha, id, url, rated, game["variant"], game["perf"], resultado, is_win, is_draw, is_lost, game["status"], segundos, color,mirank, surank, rankdiff, oponente, movidas))


def get_game_file_cache(user):
    return os.path.join(current_dir, DATA_DIR, f'games-{user}.json')

def get_data(user):
    file_name = get_game_file_cache(user)
    if not os.path.exists(file_name):
    	return {
	    "lasttimestamp": int(datetime.now().timestamp()*1000) - 7 * 24 * 60 * 60 * 1000,
	    "games": {}
	}
    with open(file_name, "r") as f:
        return json.load(f)


def save_data(user, data):
    file_output = get_game_file_cache(user)
    with open(file_output, "w") as f:
        json.dump(data, f)

def stalk_games():
    counter = 0
    headers = get_headers('application/x-ndjson')
    for u in userslist:
        userdata = get_data(u)
        api = "https://lichess.org/api/games/user/%s?pgnInJson=1&since=%s" % (u, userdata["lasttimestamp"])
        r = requests.get(url=api, headers=headers, params={})
        if r.status_code == 429:
            print("Espero un cacho porque lichess me banea: %s segundos." % ESPERA_ENTRE_429)
            sleep(ESPERA_ENTRE_429)
            r = requests.get(url = api, headers=headers)
    
        if not r.ok:
            print(r)
            print("Tengo que parar porque da erroes")
            exit(1)
 
        games = 0
        for line in r.iter_lines():
             data = json.loads(line)
             if data["id"] in userdata["games"]:
                 print("El juego %s ya estaba en %s." %(data["id"], u))
             userdata["games"][data["id"]] = data
             if userdata["lasttimestamp"] < data["createdAt"]:
                 userdata["lasttimestamp"] = 1 + int(data["createdAt"])
             games += 1

        print ("Salvando datos de %s. Ultimo: %s. Leidos: %s" % (u, datetime.fromtimestamp(userdata["lasttimestamp"]/1000), games))
        save_data(u, userdata)
        counter += 1
        if counter != len(userslist):
             sleep(ESPERA_ENTRE_BUSQUEDA)



stalk_games()
build_output()

# vim: et sts=4 ts=4
