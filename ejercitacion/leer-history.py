import json
import requests
import os
import re
import sys
from pathlib import Path
from datetime import datetime
from dateutil.relativedelta import relativedelta
from time import sleep
from common import read_listaalumnos, get_headers

# Directorio de datos. Relativo al script actual.
DATA_DIR = "../data"

# Archivo de salida. Ojo! Lo sobreescribe
FILE_OUTPUT = "history.csv"

# Este parámetro es para no bombardear la API de lichess y no bloquee
# Está en segundos
ESPERA_ENTRE_BUSQUEDA = 0.15

# Espera luego de que lichess me diga "too_many_request"
# Está en segundos
ESPERA_ENTRE_429 = 75

# Cantidad de meses de antigüedad para generar la salida
MESES_ANTIGUEDAD = 10


# Validamos el ingreso
if len(sys.argv) != 2:
    print("Uso:")
    print("  %s Lista,de,usuarios,separados,por,comas,sin,espacios\n" % sys.argv[0])
    print("Ej:")
    print("  %s juancruzariastdf,dxargon" % sys.argv[0])
    sys.exit(1)

# Paso todo a archivos
current_dir = Path(__file__).parent
file_output = os.path.join(current_dir, DATA_DIR, FILE_OUTPUT)

# Lista de usuarios a stalkear, separado por comas
users=sys.argv[1]
if users == '-':
   userslist=read_listaalumnos()
else:
   userslist=re.split(',', users.lower())

def build_output():
    fechacorte = datetime.today() - relativedelta(months=MESES_ANTIGUEDAD)
    fechacorte = fechacorte.replace(hour=0, minute=0, second=0, microsecond=0, day=1).timestamp()
    print ("Construyendo el archivo de salida %s" % file_output)
    with open(file_output, "w") as f:
        f.write("Usuario,Variante,Fecha,Rating,Mes\n")
        for u in userslist:
            data = get_data(u)
            for variante in data:
                for fechat in data[variante]:
                    if (int(fechat)<fechacorte):
                        continue
                    fecha = datetime.fromtimestamp(int(fechat)).strftime("%Y-%m-%d")
                    fechames = datetime.fromtimestamp(int(fechat)).replace(day=1).strftime("%Y-%m-%d")
                    f.write("%s,%s,%s,%s,%s\n" % (u, variante, fecha, data[variante][fechat], fechames))

def get_game_file_cache(user):
    return os.path.join(current_dir, DATA_DIR, f'history-{user}.json')

def get_data(user):
    file_name = get_game_file_cache(user)
    if not os.path.exists(file_name):
    	return { }
    with open(file_name, "r") as f:
        return json.load(f)

def save_data(user, data):
    file_output = get_game_file_cache(user)
    with open(file_output, "w") as f:
        json.dump(data, f)

def stalk_history():
    counter = 0
    headers = get_headers()
    for u in userslist:
        userdata = get_data(u)
        api = "https://lichess.org/api/user/%s/rating-history"% u
        r = requests.get(url=api, headers=headers, params={})
        if r.status_code == 429:
            print("Espero un cacho porque lichess me banea: %s segundos." % ESPERA_ENTRE_429)
            sleep(ESPERA_ENTRE_429)
            r = requests.get(url = api, headers=headers)
    
        if not r.ok:
            print(r)
            print("Tengo que parar porque da erroes con %s" % u)
            exit(1)
 
        data = r.json()
        for variante in data:
            name = variante["name"]
            if not name in userdata:
                userdata[name] = {}
            for points in variante["points"]:
                year = points[0]
                month = points[1] + 1
                day = points[2]
                fecha = str(int(datetime(year, month, day).timestamp()))
                userdata[name][fecha] = points[3]


        print ("Salvando datos de %s." % (u))
        save_data(u, userdata)
        counter += 1
        if counter != len(userslist):
             sleep(ESPERA_ENTRE_BUSQUEDA)



stalk_history()
build_output()

# vim: et sts=4 ts=4
