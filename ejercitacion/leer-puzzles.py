import json
import requests
import os
import re
import sys
from pathlib import Path
from datetime import datetime
from time import sleep
from common import read_listaalumnos, get_headers

# Directorio de datos. Relativo al script actual.
DATA_DIR = "../data"

# Archivo de salida. Ojo! Lo sobreescribe
FILE_OUTPUT = "stalkeame.csv"

# Archivo de caché, donde están guardados todos los datos anteriores.
FILE_CACHE = "stalkeame.json"

# Este parámetro es para no bombardear la API de lichess y no bloquee
# Está en segundos
ESPERA_ENTRE_BUSQUEDA = 3.05

# Espera luego de que lichess me diga "too_many_request"
# Está en segundos
ESPERA_ENTRE_429 = 75

# Validamos el ingreso
if len(sys.argv) != 2:
    print("Uso:")
    print("  %s Lista,de,usuarios,separados,por,comas,sin,espacios\n" % sys.argv[0])
    print("Ej:")
    print("  %s juancruzariastdf,dxargon" % sys.argv[0])
    sys.exit(1)

# Paso todo a archivos
current_dir = Path(__file__).parent
file_output = os.path.join(current_dir, DATA_DIR, FILE_OUTPUT)
file_cache = os.path.join(current_dir,  DATA_DIR, FILE_CACHE)

# Lista de usuarios a stalkear, separado por comas
users=sys.argv[1]
if users == '-':
   userslist=read_listaalumnos()
else:
   userslist=re.split(',', users.lower())

if len(userslist) > 300:
    print("Error: Este script es simple y no podrá procesar más de 300 usuarios.")
    print("Invito a modificar la función \"build_user_data\" para que no sea una limitante.")
    sys.exit(1)

alldata = None
userdata = {}

def build_user_data():
    print("Obteniendo datos de todos los usuarios")
    api = "https://lichess.org/api/users"
    r = requests.post(url = api, data = users.lower())
    data = r.json()
    for u in data:
       try:
           userdata[u["id"].lower()] = u['perfs']['puzzle']['games']
       except:
           print("No hay datos de puzzles para %s" % u["id"])

def build_output():
    print ("Construyendo el archivo de salida %s" % file_output)
    with open(file_output, "w") as f:
        f.write("Usuario,Fecha,Games,Rating,Wins,Loss,Diff\n")
        for u in alldata:
            for fecha, data in alldata[u].items():
                hasta = datetime.fromtimestamp(int(fecha)/ 1000)
                games = userdata[u] if u in userdata else ''
                diff = data["d"] if "d" in data else 0
                f.write("%s,%s,%s,%s,%s,%s,%s\n" % (u, hasta, games, data["r"], data["w"], data["l"], diff))

def save_data():
    with open(file_cache, "w") as f:
        json.dump(alldata, f)

def stalk_actividades():
    headers = get_headers()
    for u in userslist:
        if not u in alldata:
            alldata[u] = {}
        api = "https://lichess.org/api/user/%s/activity" % u
        r = requests.get(url = api, headers=headers)
        if r.status_code == 429:
            print("Espero un cacho porque lichess me banea: %s segundos." % ESPERA_ENTRE_429)
            sleep(ESPERA_ENTRE_429)
            r = requests.get(url = api, headers=headers)
    
        if not r.ok:
            print(r)
            print("Tengo que parar porque da erroes")
            exit(1)
    
        data = r.json()
        for intervalo in data:
            try:
                hasta = intervalo["interval"]["end"]
            except:
                print("No puedo obtener datos de %s"%u)
                print(r)
                continue
            hastadate = datetime.fromtimestamp(hasta / 1000)
            hasta = f'{hasta}'
            if "puzzles" in intervalo:
                wins = intervalo["puzzles"]["score"]["win"]
                loss = intervalo["puzzles"]["score"]["loss"]
                rank = intervalo["puzzles"]["score"]["rp"]["after"]
                ranki = intervalo["puzzles"]["score"]["rp"]["before"]
            else:
                print("%s no tiene nada para %s" %(u, hastadate))
                wins = 0
                loss = 0
                rank = 0
                ranki = 0
            alldata[u][hasta] = {"w": wins, "l": loss, "r": rank, "d": rank - ranki }
    
        print ("Salvando datos de %s" % u)
        save_data()
        sleep(ESPERA_ENTRE_BUSQUEDA)


if os.path.exists(file_cache):
    with open(file_cache, "r") as f:
        alldata = json.load(f)
else:
    alldata = {}


build_user_data()
stalk_actividades()
build_output()
