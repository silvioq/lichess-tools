<?php

$cacheFile = __DIR__ . '/../data/cache.users';
if (file_exists($cacheFile)) {
  $cacheUsers = (array)json_decode(file_get_contents($cacheFile));
} else {
  $cacheUsers = [];
}

function getBlits($username) {
   global $cacheUsers;
   global $cacheFile ;

   if (isset($cacheUsers[$username])) {
      return $cacheUsers[$username]->perfs->blitz->rating ?? '';
   }
   $url = "https://lichess.org/api/user/{$username}";
   $data = file_get_contents($url);
   $data = json_decode($data);

   $cacheUsers[$username] = $data;
   file_put_contents($cacheFile, json_encode($cacheUsers));
   return $cacheUsers[$username]->perfs->blitz->rating ?? '';
}

$argv = $_SERVER["argv"];
array_shift($argv);

fputcsv(STDOUT, [
    "ID Torneo", "Nombre Torneo", "Fecha", "Jugadores", "Usuario", "Score", "Titulo", "Ranking", "Promedio", "Equipo", "Blitz",
]);
foreach ($argv as $t) {
   $url = "https://lichess.org/api/tournament/{$t}";
   $data = file_get_contents($url);
   $data = json_decode($data);
 
   $url = "https://lichess.org/api/tournament/{$t}/results";
   $f = fopen($url, "r");
   while (false !== ($row = fgets($f))) {
       $row = json_decode($row);
       fputcsv(STDOUT, [
          $data->id,
	  $data->fullName,
	  $data->startsAt,
	  $data->nbPlayers,
	  $row->username,
	  $row->score,
	  $row->title ?? '',
	  $row->rank,
	  $row->performance ?? '',
	  $row->team,
	  'clases-de-ajedrez-en-vivo-del-imd' === $row->team ? getBlits($row->username) : '',
       ]);
   }
}


